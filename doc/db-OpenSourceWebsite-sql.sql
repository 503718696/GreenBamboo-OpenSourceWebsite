drop database if exists OSW_DB;
CREATE DATABASE OSW_DB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

use OSW_DB;

-- ----------------------------

-- Table structure for `TBUser`

-- ----------------------------

DROP TABLE IF EXISTS `TBUser`;

CREATE TABLE `TBUser` (
  `idUser` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `userName` varchar(64) COMMENT '用户名',
  `userPassword` varchar(128) COMMENT '用户密码',
  PRIMARY KEY (`idUser`)
) DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------

-- Table structure for `TBProfile`

-- ----------------------------

DROP TABLE IF EXISTS `TBProfile`;

CREATE TABLE `TBProfile` (
  `idProfile` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `profileRealName` varchar(32) COMMENT '真实名称',
  `profileSummary` varchar(128) COMMENT '简介',
   `profileValues` varchar(128) COMMENT '价值观',
  PRIMARY KEY (`idProfile`)
) DEFAULT CHARSET=utf8 COMMENT='个人档案表';



-- ----------------------------

-- Table structure for `TBPhoto`

-- ----------------------------

DROP TABLE IF EXISTS `TBPhoto`;

CREATE TABLE `TBPhoto` (
  `idPhoto` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `photoUrl` tinytext COMMENT '照片链接地址',
  `photoDescription` varchar(128) COMMENT '描述',
  `photoStory` text COMMENT '照片故事',
  `photoTime` datetime COMMENT '照片上传时间',
  PRIMARY KEY (`idPhoto`)
) DEFAULT CHARSET=utf8 COMMENT='个人档案表';



-- ----------------------------

-- Table structure for `TBProfile_TBPhoto`

-- ----------------------------

DROP TABLE IF EXISTS `TBProfile_TBPhoto`;

CREATE TABLE `TBProfile_TBPhoto` (
  `idProfile` int NOT NULL COMMENT'档案ID',
  `idPhoto` int NOT NULL COMMENT'照片ID',
  PRIMARY KEY (`idProfile`)
) DEFAULT CHARSET=utf8 COMMENT='个人档案表与照片表关联表';



-- ----------------------------

-- Table structure for `TBVideo`

-- ----------------------------

DROP TABLE IF EXISTS `TBVideo`;

CREATE TABLE `TBVideo` (
  `idVideo` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `videoUrl` tinytext COMMENT '视频链接地址',
  `videoTitle` varchar(128) COMMENT '视频标题',
  `videoDescription` tinytext COMMENT '视频描述',
  `videoCaption` text COMMENT '视频字幕',
  `videoTime` datetime COMMENT '视频上传时间',
  PRIMARY KEY (`idVideo`)
) DEFAULT CHARSET=utf8 COMMENT='视频表';


-- ----------------------------

-- Table structure for `TBProfile_TBVideo`

-- ----------------------------

DROP TABLE IF EXISTS `TBProfile_TBVideo`;

CREATE TABLE `TBProfile_TBVideo` (
  `idProfile` int NOT NULL COMMENT '档案ID',
  `idVideo` int NOT NULL  COMMENT '视频ID',
  PRIMARY KEY (`idProfile`)
) DEFAULT CHARSET=utf8 COMMENT='个人档案表与视频表关联表';



-- ----------------------------

-- Table structure for `TBProject`

-- ----------------------------

DROP TABLE IF EXISTS `TBProject`;

CREATE TABLE `TBProject` (
  `idProject` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `projectName` varchar(128) COMMENT '项目名称',
  `projectSummary` text COMMENT '项目简介',
  PRIMARY KEY (`idProject`)
) DEFAULT CHARSET=utf8 COMMENT='项目表';


-- ----------------------------

-- Table structure for `TBProfile_TBProject`

-- ----------------------------

DROP TABLE IF EXISTS `TBProfile_TBProject`;

CREATE TABLE `TBProfile_TBProject` (
  `idProfile` int NOT NULL COMMENT '档案ID',
  `idProject` int NOT NULL COMMENT '项目ID',
  PRIMARY KEY (`idProfile`)
) DEFAULT CHARSET=utf8 COMMENT='个人档案表与项目表关联表';

-- ----------------------------

-- Table structure for `TBProject_TBVideo`

-- ----------------------------

DROP TABLE IF EXISTS `TBProject_TBVideo`;

CREATE TABLE `TBProject_TBVideo` (
  `idProject` int NOT NULL COMMENT '项目ID',
  `idVideo` int NOT NULL  COMMENT '视频ID',
  PRIMARY KEY (`idProject`)
) DEFAULT CHARSET=utf8 COMMENT='项目表与视频表关联表';


-- ----------------------------

-- Table structure for `TBProject_TBPhoto`

-- ----------------------------

DROP TABLE IF EXISTS `TBProject_TBPhoto`;

CREATE TABLE `TBProject_TBPhoto` (
  `idProject` int NOT NULL COMMENT '项目ID',
  `idPhoto` int NOT NULL COMMENT '照片ID',
  PRIMARY KEY (`idProject`)
) DEFAULT CHARSET=utf8 COMMENT='项目表与照片表关联表';

-- ----------------------------

-- Table structure for `TBContribution`

-- ----------------------------

DROP TABLE IF EXISTS `TBContribution`;

CREATE TABLE `TBContribution` (
  `idContribution` int NOT NULL COMMENT '贡献度表ID',
  `contributionTotal` int NOT NULL COMMENT '贡献度总数',
  PRIMARY KEY (`idContribution`)
) DEFAULT CHARSET=utf8 COMMENT='贡献度表';



-- ----------------------------

-- Table structure for `TBProfile_TBContribution`

-- ----------------------------

DROP TABLE IF EXISTS `TBProfile_TBContribution`;

CREATE TABLE `TBProfile_TBContribution` (
  `idProfile` int NOT NULL COMMENT '档案ID',
  `idContribution` int NOT NULL COMMENT '贡献度ID',
  PRIMARY KEY (`idProfile`)
) DEFAULT CHARSET=utf8 COMMENT='个人档案表与贡献度表关联表';


-- ----------------------------

-- Table structure for `TBActivity`

-- ----------------------------

DROP TABLE IF EXISTS `TBActivity`;

CREATE TABLE `TBActivity` (
  `idActivity` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `activitySponsor` VARCHAR(128) COMMENT '活动发起人',
  `activityLocation` tinytext COMMENT '活动地点',
  `activityTheme` VARCHAR(128) COMMENT '活动主题',
  `activityTime` datetime COMMENT '活动时间',
  `activityOpenTime` datetime COMMENT '发起时间',
  PRIMARY KEY (`idActivity`)
) DEFAULT CHARSET=utf8 COMMENT='活动表';

-- ----------------------------

-- Table structure for `TBActivity_TBPhoto`

-- ----------------------------

DROP TABLE IF EXISTS `TBActivity_TBPhoto`;

CREATE TABLE `TBActivity_TBPhoto` (
  `idActivity` int NOT NULL COMMENT '活动ID',
  `idPhoto` int NOT NULL COMMENT '照片ID',
  PRIMARY KEY (`idActivity`)
) DEFAULT CHARSET=utf8 COMMENT='活动表与照片表关联表';

-- ----------------------------

-- Table structure for `TBActivity_TBVideo`

-- ----------------------------

DROP TABLE IF EXISTS `TBActivity_TBVideo`;

CREATE TABLE `TBActivity_TBVideo` (
  `idActivity` int NOT NULL COMMENT '活动ID',
  `idVideo` int NOT NULL COMMENT '视频ID',
  PRIMARY KEY (`idActivity`)
) DEFAULT CHARSET=utf8 COMMENT='活动表与视频表关联表';

-- ----------------------------

-- Table structure for `TBCapital`

-- ----------------------------

DROP TABLE IF EXISTS `TBCapital`;

CREATE TABLE `TBCapital` (
  `idCapital` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  `capitalMoney` double COMMENT '资金金额',
  `capitalDescription` tinytext COMMENT '交易描述',
  `capitalTradingAccount` VARCHAR(128) COMMENT '交易账号',
  `capitalTime` datetime COMMENT '交易时间',
  PRIMARY KEY (`idCapital`)
) DEFAULT CHARSET=utf8 COMMENT='资金表';


-- ----------------------------

-- Table structure for `TBReserved`

-- ----------------------------

DROP TABLE IF EXISTS `TBReserved`;

CREATE TABLE `TBReserved` (
  `idReserved` int NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
  PRIMARY KEY (`idReserved`)
) DEFAULT CHARSET=utf8 COMMENT='资金表';
